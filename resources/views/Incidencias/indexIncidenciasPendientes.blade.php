@extends('layouts.app')

@section('content')
<div style="padding: 10px;">

    <div class="pt-3 pb-3">
        @if(Auth::user()->role_id < 3) <a class="btn btn-warning" data-toggle="tooltip" href="{{route('incidencias')}}" title="Nuevo Registro">
            <i class="fas fa-plus">
            </i>
            Listado de Incidencias
            </a>
            @endif
    </div>

    <div class="card">
        <div class="card-header text-center">
            <strong><span style="font-size: 20px;">MIS ASIGNACIONES</span></strong>
        </div>
        <div class="card-body">
            <table id="tabledata" class="table table-bordered table-hover table-sm">
                <thead>
                    <tr>
                        <th class="text-center" width="40px">
                            NRO
                        </th>
                        <th class="text-center" width="250px">
                            ESTUDIANTE
                        </th>
                        <th class="text-center" width="180px">
                            FECHA
                        </th>
                        <th class="text-center">
                            TITULO
                        </th>
                        <th class="text-center">
                            DESCRIPCION
                        </th>
                        <th class="text-center">
                            ESTADO
                        </th>
                        @if(Auth::user()->role_id < 3) <th>
                            </th>
                            <th></th>
                            @endif
                    </tr>
                </thead>
                <?php

                use Illuminate\Support\Facades\Auth;

                $a = 1; ?>
                <tbody>
                    @foreach($incidencias as $inci)
                    <tr>
                        <td class="text-center"> <?php echo "$a"; ?> </td>
                        <td class="text-center">
                            {{ $inci->estudiante($inci->user_id)}}
                        </td>
                        <td class="text-center">
                            <?php echo date_format($inci->created_at, "d/m/Y") ?>
                        </td>
                        <td class="text-center">
                            {{ $inci->titulo}}
                        </td>
                        <td class="text-center">
                            {{ $inci->descripcion}}
                        </td>
                        <td class="text-center">
                            @if($inci->estado_id == 1)
                            <span class="badge badge-danger">{{$inci->estado->nombre}}</span>
                            @elseif($inci->estado_id == 2)
                            <span class="badge badge-success">{{$inci->estado->nombre}}</span>
                            @else
                            <span class="badge badge-warning">{{$inci->estado->nombre}}</span>
                            @endif
                        </td>

                        @if(Auth::user()->role_id < 3) <td class="text-center" width="5px">
                            <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#asignar" data-id="{{$inci->id}}"><i class="bi bi-check2-square"></i></a>
                            </td>
                            @endif
                            <td class="text-center" width="5px">
                                <a class="btn btn-warning btn-sm" data-toggle="tooltip" href="{{route('detalleIncidencia', $inci->id)}}" title="Detalles de la Incidencia">
                                    <i class="bi bi-card-list"></i>
                                </a>
                            </td>
                    </tr>
                    <?php $a++; ?>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<br>

<!-- Modal -->
<div class="modal fade" id="asignar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header bg-light">
                <h5 class="modal-title" id="exampleModalLabel">Atender Incidencia</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('postDetalleIncidencia') }}" id="formAsignar">
                @csrf
                <div class="modal-body">

                    <div>
                        <input type="hidden" class="form-control" id="id" name="incidencia_id">
                    </div>
                    <div>
                        <label for="estado_id" class="col-form-label">Estado:</label>
                        <select name="estado_id" id="estado_id" class="form-control"></select>
                    </div>

                    <div>
                        <label for="descripcion" class="col-form-label">Nota</label>
                        <textarea name="descripcion" id="descripcion" class="form-control" rows="5" cols="50" value="{{ old('descripcion') }}" required autocomplete="descripcion" placeholder="Describa la Incidencia.."></textarea>
                    </div>
                                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Confirmar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Fin del Modal -->

<script type="application/javascript">
    $('#asignar').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) //se realiza el evento
        var incidencia_id = button.data('id')
        
        var modal = $(this)
        modal.find('.modal-body #id').val(incidencia_id)
            
        $.get('{{route('estados')}}', function(data){               
                var html_select = '<option value = ""> Seleccione el un estado...</option>';
                for(var i=0; i<data.length; ++i)
                    html_select += '<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                $('#estado_id').html(html_select);
			});
    });
</script>

@endsection