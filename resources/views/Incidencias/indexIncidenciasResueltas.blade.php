@extends('layouts.app')

@section('content')
<div style="padding: 10px;">

    <div class="pt-3 pb-3">
        @if(Auth::user()->role_id < 3) <a class="btn btn-warning" data-toggle="tooltip" href="{{route('incidencias')}}" title="Nuevo Registro">
            <i class="fas fa-plus">
            </i>
            Listado de Incidencias
            </a>
            @endif
    </div>

    <div class="card">
        <div class="card-header text-center">
            <strong><span style="font-size: 20px;">MIS ASIGNACIONES</span></strong>
        </div>
        <div class="card-body">
            <table id="tabledata" class="table table-bordered table-hover table-sm">
                <thead>
                    <tr>
                        <th class="text-center" width="40px">
                            NRO
                        </th>
                        <th class="text-center" width="250px">
                            ESTUDIANTE
                        </th>
                        <th class="text-center" width="180px">
                            FECHA
                        </th>
                        <th class="text-center">
                            TITULO
                        </th>
                        <th class="text-center">
                            DESCRIPCION
                        </th>
                        <th class="text-center">
                            ESTADO
                        </th>
                        @if(Auth::user()->role_id < 3) 
                        <th>
                        </th>
                        @endif
                    </tr>
                </thead>
                <?php

                use Illuminate\Support\Facades\Auth;

                $a = 1; ?>
                <tbody>
                    @foreach($incidencias as $inci)
                    <tr>
                        <td class="text-center"> <?php echo "$a"; ?> </td>
                        <td class="text-center">
                            {{ $inci->estudiante($inci->user_id)}}
                        </td>
                        <td class="text-center">
                            <?php echo date_format($inci->created_at, "d/m/Y") ?>
                        </td>
                        <td class="text-center">
                            {{ $inci->titulo}}
                        </td>
                        <td class="text-center">
                            {{ $inci->descripcion}}
                        </td>
                        <td class="text-center">
                            @if($inci->estado_id == 1)
                            <span class="badge badge-danger">{{$inci->estado->nombre}}</span>
                            @elseif($inci->estado_id == 2)
                            <span class="badge badge-success">{{$inci->estado->nombre}}</span>
                            @else
                            <span class="badge badge-warning">{{$inci->estado->nombre}}</span>
                            @endif
                        </td>

                        <td class="text-center" width="5px">
                            <a class="btn btn-warning btn-sm" data-toggle="tooltip" href="{{route('detalleIncidencia', $inci->id)}}" title="Detalles de la Incidencia">
                                <i class="bi bi-card-list"></i>
                            </a>
                        </td>
                    </tr>
                    <?php $a++; ?>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<br>

@endsection