@extends('layouts.app')

@section('content')
<div style="padding: 10px;">

    <div class="pt-3 pb-3">
        @if(Auth::user()->role_id == 3)
        <a class="btn btn-success" data-toggle="tooltip" href="{{route('registrarIncidencia')}}" title="Nuevo Registro">
            <i class="fas fa-plus">
            </i>
            Nuevo Registro
        </a>
        @endif
        @if(Auth::user()->role_id == 2)
        <a class="btn btn-info" data-toggle="tooltip" href="{{route('indexIncidenciasPendientes')}}" title="Nuevo Registro">
            <i class="fas fa-plus">
            </i>
            Mis Asignaciones Pendientes
        </a>
        <a class="btn btn-success" data-toggle="tooltip" href="{{route('indexIncidenciasResueltas')}}" title="Nuevo Registro">
            <i class="fas fa-plus">
            </i>
            Mis Asignaciones Atentidas
        </a>
        @endif
    </div>
    <div class="card card-outline card-success">
        <div class="card-header d-flex">
            <strong><span style="font-size: 20px;">INCIDENCIAS</span></strong>
            <ul class="nav nav-pills ml-auto p-2">

                <?php

                    use Illuminate\Support\Facades\Auth;

                    $estado1 = Auth::user()->role_id == 3 ?  'active' : '';
                    $estado2 = Auth::user()->role_id < 3 ?  'active' : '';
                ?>

                @if(Auth::user()->role_id < 3)
               
                <li class="nav-item"><a class="nav-link <?php echo $estado2; ?>" href="#tab_1" data-toggle="tab">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">No Asignadas</font>
                        </font>
                    </a>
                </li>
                @endif

                <li class="nav-item"><a class="nav-link <?php echo $estado1; ?>" href="#tab_2" data-toggle="tab">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;" id="preventivo">No Resueltas</font>
                        </font>
                    </a>
                </li>
                <li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;" id="preventivo">Resueltas</font>
                        </font>
                    </a>
                </li>
                <li class="nav-item"><a class="nav-link" href="#tab_4" data-toggle="tab">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;" id="preventivo">Pausadas</font>
                        </font>
                    </a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content">
                
                @if(Auth::user()->role_id < 3)
                <!-- incidencias no asignadas -->
                <div class="tab-pane <?php echo $estado2; ?> table-responsive" id="tab_1">
                    <table id="tabledata" class="table table-bordered table-hover table-sm">
                        <thead>
                            <tr>
                                <th class="text-center" width="40px">
                                    NRO
                                </th>
                                <th class="text-center" width="250px">
                                    ESTUDIANTE
                                </th>
                                @if(Auth::user()->role_id != 3)
                                <th class="text-center" width="250px">
                                    CARRERA
                                </th>
                                @endif
                                <th class="text-center" width="180px">
                                    FECHA
                                </th>
                                <th class="text-center">
                                    TITULO
                                </th>
                                <th class="text-center">
                                    DESCRIPCION
                                </th>                                
                                @if(Auth::user()->role_id == 2)
                                    <th></th>
                                @endif
                                    <th></th>
                                
                            </tr>
                        </thead>
                        <?php

                        $a = 1; ?>
                        <tbody>
                            @foreach($noAsignadas as $no)
                            <tr>
                                <td class="text-center"> <?php echo "$a"; ?> </td>
                                <td class="text-center">
                                    {{ $no->estudiante($no->user_id)}}
                                </td>
                                @if(Auth::user()->role_id != 3)
                                <td class="text-center">
                                    {{ $no->carrera($no->carrera_id)}}
                                </td>
                                @endif
                                <td class="text-center">
                                    <?php echo date_format($no->created_at,"d/m/Y") ?>
                                </td>
                                
                                <td class="text-center">
                                    {{ $no->titulo}}
                                </td>                                
                                <td class="text-center">
                                    {{ $no->descripcion}}
                                </td>                             

                                @if(Auth::user()->role_id == 2) 
                                <td class="text-center" width="5px">
                                    <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#asignar" data-id="{{$no->id}}"><i class="bi bi-calendar2-check"></i></a>
                                </td>
                                @endif
                                    <td class="text-center" width="5px">
                                        <a class="btn btn-warning btn-sm" data-toggle="tooltip" href="{{route('detalleIncidencia', $no->id)}}" title="Detalles de la Incidencia">
                                            <i class="bi bi-card-list"></i>
                                        </a>
                                    </td>
                            </tr>
                            <?php $a++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- ==================== -->
                @endif

                <!-- incidencias no resueltas -->
                <div class="tab-pane <?php echo $estado1; ?> table-responsive" id="tab_2">
                    <table id="tabledata" class="table table-bordered table-hover table-sm">
                        <thead>
                            <tr>
                                <th class="text-center" width="40px">
                                    NRO
                                </th>
                                <th class="text-center" width="250px">
                                    ESTUDIANTE
                                </th>
                                @if(Auth::user()->role_id != 3)
                                <th class="text-center" width="250px">
                                    CARRERA
                                </th>
                                @endif
                                <th class="text-center" width="180px">
                                    FECHA
                                </th>
                                <th class="text-center">
                                    TITULO
                                </th>
                                <th class="text-center">
                                    DESCRIPCION
                                </th>
                                <th class="text-center">
                                    ESTADO
                                </th>
                                
                                <th></th>
                              
                            </tr>
                        </thead>
                        <?php

                        $a = 1; ?>
                        <tbody>
                            @foreach($noResueltas as $noRe)
                            <tr>
                                <td class="text-center"> <?php echo "$a"; ?> </td>
                                <td class="text-center">
                                    {{ $noRe->estudiante($noRe->user_id)}}
                                </td>
                                @if(Auth::user()->role_id != 3)
                                <td class="text-center">
                                    {{ $noRe->carrera($noRe->carrera_id)}}
                                </td>
                                @endif
                                <td class="text-center">
                                    <?php echo date_format($noRe->created_at,"d/m/Y") ?>
                                </td>
                                <td class="text-center">
                                    {{ $noRe->titulo}}
                                </td>
                                <td class="text-center">
                                    {{ $noRe->descripcion}}
                                </td>
                                <td class="text-center">                                   
                                    <span class="badge badge-danger">{{$noRe->estado->nombre}}</span>                                    
                                </td>
                                                            
                                <td class="text-center" width="5px">
                                    <a class="btn btn-warning btn-sm" data-toggle="tooltip" href="{{route('detalleIncidencia', $noRe->id)}}" title="Detalles de la Incidencia">
                                        <i class="bi bi-card-list"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php $a++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--===================  -->

                <!-- incidencias resueltas -->
                <div class="tab-pane table-responsive" id="tab_3">
                    <table id="tabledata" class="table table-bordered table-hover table-sm">
                        <thead>
                            <tr>
                                <th class="text-center" width="40px">
                                    NRO
                                </th>
                                <th class="text-center" width="250px">
                                    ESTUDIANTE
                                </th>
                                @if(Auth::user()->role_id != 3)
                                <th class="text-center" width="250px">
                                    CARRERA
                                </th>
                                @endif
                                <th class="text-center" width="180px">
                                    FECHA
                                </th>
                                <th class="text-center">
                                    TITULO
                                </th>
                                <th class="text-center">
                                    DESCRIPCION
                                </th>
                                <th class="text-center">
                                    ESTADO
                                </th>
                                
                                <th></th>
                              
                            </tr>
                        </thead>
                        <?php

                        $a = 1; ?>
                        <tbody>
                            @foreach($resueltas as $Re)
                            <tr>
                                <td class="text-center"> <?php echo "$a"; ?> </td>
                                <td class="text-center">
                                    {{ $Re->estudiante($Re->user_id)}}
                                </td>
                                @if(Auth::user()->role_id != 3)
                                <td class="text-center">
                                    {{ $Re->carrera($Re->carrera_id)}}
                                </td>
                                @endif
                                <td class="text-center">
                                    <?php echo date_format($Re->created_at,"d/m/Y") ?>
                                </td>
                                <td class="text-center">
                                    {{ $Re->titulo}}
                                </td>
                                <td class="text-center">
                                    {{ $Re->descripcion}}
                                </td>
                                <td class="text-center">                                   
                                    <span class="badge badge-success">{{$Re->estado->nombre}}</span>                                    
                                </td>                              
                                <td class="text-center" width="5px">
                                    <a class="btn btn-warning btn-sm" data-toggle="tooltip" href="{{route('detalleIncidencia', $Re->id)}}" title="Detalles de la Incidencia">
                                        <i class="bi bi-card-list"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php $a++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--===================  -->

                <!-- incidencias pausadas -->
                <div class="tab-pane table-responsive" id="tab_4">
                    <table id="tabledata" class="table table-bordered table-hover table-sm">
                        <thead>
                            <tr>
                                <th class="text-center" width="40px">
                                    NRO
                                </th>
                                <th class="text-center" width="250px">
                                    ESTUDIANTE
                                </th>
                                @if(Auth::user()->role_id != 3)
                                <th class="text-center" width="250px">
                                    CARRERA
                                </th>
                                @endif
                                <th class="text-center" width="180px">
                                    FECHA
                                </th>
                                <th class="text-center">
                                    TITULO
                                </th>
                                <th class="text-center">
                                    DESCRIPCION
                                </th>
                                <th class="text-center">
                                    ESTADO
                                </th>
                                
                                <th></th>
                              
                            </tr>
                        </thead>
                        <?php

                        $a = 1; ?>
                        <tbody>
                            @foreach($pausadas as $pa)
                            <tr>
                                <td class="text-center"> <?php echo "$a"; ?> </td>
                                <td class="text-center">
                                    {{ $pa->estudiante($pa->user_id)}}
                                </td>
                                @if(Auth::user()->role_id != 3)
                                <td class="text-center">
                                    {{ $pa->carrera($pa->carrera_id)}}
                                </td>
                                @endif
                                <td class="text-center">
                                    <?php echo date_format($pa->created_at,"d/m/Y") ?>
                                </td>
                                <td class="text-center">
                                    {{ $pa->titulo}}
                                </td>
                                <td class="text-center">
                                    {{ $pa->descripcion}}
                                </td>
                                <td class="text-center">
                                    <span class="badge badge-warning">{{$pa->estado->nombre}}</span>                                   
                                </td>

                                <td class="text-center" width="5px">
                                    <a class="btn btn-warning btn-sm" data-toggle="tooltip" href="{{route('detalleIncidencia', $pa->id)}}" title="Detalles de la Incidencia">
                                        <i class="bi bi-card-list"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php $a++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--===================  -->
            </div>
        </div>
    </div>
</div>
<br>

<!-- Modal -->
<div class="modal fade" id="asignar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header bg-light">
                <h5 class="modal-title" id="exampleModalLabel">Confirmación</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('postAsignarIncidencia') }}" id="formAsignar">
                @csrf
                <div class="modal-body">
                    <strong>Está seguro de Asignarse esta Incidencia?</strong>
                    <div>
                        <input type="hidden" class="form-control" id="id" name="incidencia_id">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Confirmar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Fin del Modal -->

<script type="application/javascript">
    $('#asignar').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) //se realiza el evento
        var incidencia_id = button.data('id')
        console.log(incidencia_id)
        var modal = $(this)
        modal.find('.modal-body #id').val(incidencia_id)
    });
</script>


@endsection