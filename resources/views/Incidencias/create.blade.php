@extends('layouts.app')

@section('content')

<div class="container pt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">
                    <strong><span style="font-size: 20px;">{{ __('Registrar Incidencia') }}</span></strong>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('postRegistrarIncidencia') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="estudiante" class="col-md-3 col-form-label text-md-right">{{ __('Estudiante') }}</label>

                            <div class="col-md-9">
                                <input id="estudiante" type="text" class="form-control" name="estudiante" value="{{ $estudiante->nombrecompleto() }}" autocomplete="estudiante" disabled>
                                
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="titulo" class="col-md-3 col-form-label text-md-right">{{ __('Titulo') }}</label>

                            <div class="col-md-9">
                                <input id="titulo" type="text" class="form-control @error('titulo') is-invalid @enderror" name="titulo" value="{{ old('titulo') }}" required autocomplete="titulo" autofocus>

                                @if ($errors->has('titulo'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('titulo') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="descripcion" class="col-md-3 col-form-label text-md-right">{{ __('Descripción') }}</label>

                            <div class="col-md-9">
                                <textarea name="descripcion" id="descripcion" class="form-control @error('descripcion') is-invalid @enderror" rows="5" cols="50" value="{{ old('descripcion') }}" required autocomplete="descripcion" placeholder="Describa la Incidencia.."></textarea>
                                @if ($errors->has('descripcion'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('descripcion') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Registrar') }}
                                </button>
                                <a href="{{route ('incidencias') }}" class="btn btn-warning" data-toggle="tooltip" title="Volver al Listado">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection