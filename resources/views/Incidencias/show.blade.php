@extends('layouts.app')

@section('content')

<div class="container pt-5">
    <div class="card card-outline card-info">
        <div class="card-header">
            <strong><span style="font-size: 20px;">DETALLES DE LA INCIDENCIA</span></strong>
        </div>
        <div class="card-body">
            <dl class="row">
                <dt class="col-sm-1">
                    Fecha:
                </dt>
                <dd class="col-sm-5">
                    {{ $incidencia->created_at}}
                </dd>
                <dt class="col-sm-2 text-right">
                    Registrado:
                </dt>
                <dd class="col-sm-4">
                    {{ $incidencia->estudiante($incidencia->user_id)}}
                </dd>
            </dl>    
            <dl class="row">
                <dt class="col-sm-1">
                    Titulo:
                </dt>
                <dd class="col-sm-5">
                    {{ $incidencia->titulo}}
                </dd>
                <dt class="col-sm-2 text-right">
                    Descripción:
                </dt>
                <dd class="col-sm-4">
                    {{ $incidencia->descripcion}}
                </dd>
            </dl>    
        </div>

        <div class="card">
        <div class="card-header text-center">
            <strong><span style="font-size: 20px;">ATENCIONES</span></strong>
        </div>
        <div class="card-body">
            <table id="tabledata" class="table table-bordered table-hover table-sm">
                <thead>
                    <tr>
                        <th class="text-center" width="40px">
                            NRO
                        </th>
                        <th class="text-center" width="250px">
                            ENCARGADO
                        </th>
                        <th class="text-center" width="180px">
                            FECHA
                        </th>
                        <th class="text-center">
                            DESCRIPCIÓN
                        </th>
                        <th class="text-center" width="40px">
                            ESTADO
                        </th>
                        
                    </tr>
                </thead>
                <?php
               

                $a = 1; ?>
                <tbody>
                    @foreach($detalles as $inci)
                    <tr>
                        <td class="text-center"> <?php echo "$a"; ?> </td>
                        <td class="text-center">
                            {{ $inci->incidencia->docente($inci->incidencia->user_asignado_id)}}
                        </td>
                        <td class="text-center">
                            <?php echo date_format($inci->created_at, "d/m/Y") ?>
                        </td>
                        <td class="text-center">
                            {{ $inci->descripcion }}
                        </td>
                        <td class="text-center">
                            @if($inci->estado == 1)
                            <span class="badge badge-danger">No Resuelta</span>
                            @elseif($inci->estado == 2)
                            <span class="badge badge-success">Resuelta</span>
                            @else
                            <span class="badge badge-warning">Pausada</span>
                            @endif
                        </td>
                    </tr>
                    <?php $a++; ?>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </div>
</div>
@endsection