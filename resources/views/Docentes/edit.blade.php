@extends('layouts.app')

@section('content')
<div class="container pt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">
                    <strong><span style="font-size: 20px;">{{ __('Editar Docente') }}</span></strong>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('postEditarDocente', $docente->id) }}">
                        @csrf
                        @method('PUT')

                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control" name="nombre" value="{{ old('nombre', $docente->nombre) }}" required autocomplete="nombre" autofocus>

                                @if ($errors->has('nombre'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="apellidos" class="col-md-4 col-form-label text-md-right">{{ __('Apellidos') }}</label>

                            <div class="col-md-6">
                                <input id="apellidos" type="text" class="form-control" name="apellidos" value="{{ old('apellidos', $docente->apellidos) }}" required autocomplete="name">

                                @if ($errors->has('apellidos'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('apellidos') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="correo" class="col-md-4 col-form-label text-md-right">{{ __('Correo Electrónico') }}</label>

                            <div class="col-md-6">
                                <input id="correo" type="email" class="form-control" name="correo" value="{{ old('correo', $docente->correo) }}" required autocomplete="email">

                                @if ($errors->has('correo'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('correo') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="carnet" class="col-md-4 col-form-label text-md-right">{{ __('Carnet de Identidad') }}</label>

                            <div class="col-md-6">
                                <input id="carnet" type="text" class="form-control" name="carnet" value="{{ old('carnet', $docente->carnet) }}" required autocomplete="name">

                                @if ($errors->has('carnet'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('carnet') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="profesion_id" class="col-md-4 col-form-label text-md-right">{{ __('Profesión') }}</label>

                            <div class="col-md-6">                                
                                <select name="profesion_id" id="profesion_id" class="form-control" name="profesion_id" value="{{ old('profesion_id') }}" required autocomplete="name">
                                    <option value="">Seleccione una Carrera..</option>
                                    @foreach($profesiones as $pro)
                                    <option value="{{$pro->id}}" {{ $pro->id == $docente->profesion_id ? 'selected' : '' }} >{{$pro->nombre}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('profesion_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('profesion_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="carrera_id" class="col-md-4 col-form-label text-md-right">{{ __('Carrera') }}</label>

                            <div class="col-md-6">                                
                                <select name="carrera_id" id="carrera_id" class="form-control" name="carrera_id" value="{{ old('carrera_id') }}" required autocomplete="name">
                                    <option value="">Seleccione una Carrera..</option>
                                    @foreach($carreras as $ca)
                                    <option value="{{$ca->id}}" {{ $ca->id == $docente->carrera_id ? 'selected' : '' }} >{{$ca->nombre}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('carrera_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('carrera_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Editar') }}
                                </button>
                                <a href="{{route ('docentes') }}" class="btn btn-warning" data-toggle="tooltip" title="Volver al Listado">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection