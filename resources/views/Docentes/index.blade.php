@extends('layouts.app')

@section('content')
<div class="container">

    <div class="pt-3 pb-3">
        <a class="btn btn-success" data-toggle="tooltip" href="{{ route('registrarDocente')}}" title="Nuevo Registro">
            <i class="fas fa-plus">
            </i>
            Nuevo Registro
        </a>
    </div>

    <div class="card">
        <div class="card-header text-center">
            <strong><span style="font-size: 20px;">LISTA DE DOCENTES</span></strong>
        </div>
        <div class="card-body">
            <table id="tabledata" class="table table-bordered table-hover table-sm">
                <thead>
                    <tr>
                        <th class="text-center" width="40px">
                            NRO
                        </th>
                        <th class="text-center">
                            NOMBRE COMPLETO
                        </th>
                        <th class="text-center">
                            CORREO
                        </th>
                        <th class="text-center">
                            CARNET
                        </th>
                        <th class="text-center">
                            PROFESIÓN
                        </th>
                        <th class="text-center">
                            CARRERA
                        </th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <?php $a = 1; ?>
                <tbody>
                    @foreach($docentes as $doce)
                    <tr>
                        <td class="text-center"> <?php echo "$a"; ?> </td>
                        <td class="text-center">
                            {{ $doce->nombrecompleto()}}
                        </td>
                        <td class="text-center">
                            {{ $doce->correo}}
                        </td>
                        <td class="text-center">
                            {{ $doce->carnet}}
                        </td>
                        <td class="text-center">
                            {{ $doce->profesion->nombre }}
                        </td>
                        <td class="text-center">
                            {{ $doce->carrera->nombre }}
                        </td>
                        <td class="text-center" width="5px">
                            <a class="btn btn-primary btn-sm" data-toggle="tooltip" href="{{route('editarDocente', $doce->id)}}" title="Editar Docente">
                                <i class="bi bi-pencil-square"></i>
                            </a>
                        </td>

                        <td class="text-center" width="5px">
                            <a class="btn btn-warning btn-sm" data-toggle="tooltip" href="{{route('detalleDocente', $doce->id)}}" title="Detalles del Docente">
                                <i class="bi bi-person-lines-fill"></i>
                            </a>
                        </td>
                    </tr>
                    <?php $a++; ?>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<br>
@endsection