@extends('layouts.app')

@section('content')

<div class="container pt-5">
    <div class="card card-outline card-info">
        <div class="card-header">
            <strong><span style="font-size: 20px;">DETALLES DEL DOCENTE</span></strong>
        </div>
        <div class="card-body">
            <dl class="row">
                <dt class="col-sm-1">
                    Nombre:
                </dt>
                <dd class="col-sm-5">
                    {{ $docente->nombrecompleto()}}
                </dd>
                <dt class="col-sm-2 text-right">
                    Correo:
                </dt>
                <dd class="col-sm-4">
                    {{ $docente->correo }}
                </dd>
            </dl>    
            <dl class="row">
                <dt class="col-sm-1">
                    Profesión
                </dt>
                <dd class="col-sm-5">
                    {{ $docente->profesion->nombre}}
                </dd>
                <dt class="col-sm-2 text-right">
                    Carrera:
                </dt>
                <dd class="col-sm-4">
                    {{ $docente->carrera->nombre }}
                </dd>
            </dl>    
        </div>

    </div>
</div>
@endsection