@extends('layouts.app')

@section('content')

<div class="container pt-5">
    <div class="card card-outline card-info">
        <div class="card-header">
            <strong><span style="font-size: 20px;">DETALLES DEL ESTUDIANTE</span></strong>
        </div>
        <div class="card-body">
            <dl class="row">
                <dt class="col-sm-1">
                    Nombre:
                </dt>
                <dd class="col-sm-5">
                    {{ $estudiante->nombrecompleto()}}
                </dd>
                <dt class="col-sm-2 text-right">
                    Correo:
                </dt>
                <dd class="col-sm-4">
                    {{ $estudiante->correo }}
                </dd>
            </dl>    
            <dl class="row">
                <dt class="col-sm-1">
                    Carnet:
                </dt>
                <dd class="col-sm-5">
                    {{ $estudiante->carnet}}
                </dd>
                <dt class="col-sm-2 text-right">
                    Carrera:
                </dt>
                <dd class="col-sm-4">
                    {{ $estudiante->carrera->nombre }}
                </dd>
            </dl>    
        </div>

    </div>
</div>
@endsection