<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncidenciaDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidencia_detalles', function (Blueprint $table) {
            $table->Increments('id');
            $table->integer('incidencia_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('estado');
            $table->string('descripcion');
            $table->timestamps();

            $table->foreign('incidencia_id')->references('id')->on('incidencias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidencia_detalles');
    }
}
