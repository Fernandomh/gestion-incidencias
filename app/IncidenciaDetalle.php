<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidenciaDetalle extends Model
{
    protected $fillable = [
        "incidencia_id", "user_id", "estado", "descripcion"
    ];

    protected $table = "incidencia_detalles";

    protected $primarykey = "id";

    public function incidencia()
    {
        return $this->belongsTo(Incidencias::class, 'incidencia_id');
    }
}
