<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incidencias extends Model
{
    protected $fillable = [
        'user_id', 'user_asignado_id', 'carrera_id', 'titulo', 'descripcion', 'estado_id'
    ];

    protected $table = "incidencias";

    protected $primarykey = "id";

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function estado()
    {
        return $this->belongsTo(EstadoIncidencia::class, 'estado_id');
    }

    public function estudiante($iduser)
    {
        $estudiante = Estudiante::where('user_id', $iduser)->first();

        return $estudiante->nombre . ' ' . $estudiante->apellidos;
    }

    public function docente($iduser)
    {
        $estudiante = Docente::where('user_id', $iduser)->first();

        return $estudiante->nombre . ' ' . $estudiante->apellidos;
    }

    public function carrera($idcarrera)
    {
        $carrera = Carrera::where('id', $idcarrera)->first();

        return $carrera->nombre;
    }
}
