<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    protected $fillable = [
        'user_id', 'nombre', 'apellidos', 'correo', 'carnet', 'carrera_id'
    ];

    protected $primarykey = 'id';

    protected $table = 'estudiantes';

    public function nombrecompleto()
    {
        return $this->nombre . ' ' . $this->apellidos;
    }

    public function carrera()
    {
        return $this->belongsTo(Carrera::class, 'carrera_id');
    }
}
