<?php

namespace App\Http\Controllers;

use App\Carrera;
use App\Estudiante;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class EstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estudiantes = Estudiante::all();
        return view('Estudiantes.index', compact('estudiantes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $carreras = Carrera::all();
        return view('Estudiantes.create', compact('carreras'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::create([
            'role_id' => 3,
            'name' => $request->nombre,
            'email' => $request->correo,
            'password' => Hash::make($request->carnet),
        ]);
       
        $request->merge(['user_id' => $user->id]);       
        $estudiante = Estudiante::create($request->all());
        
        Alert::success('Registro Exitoso');
        
        return redirect()->route('estudiantes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Estudiante  $estudiante
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $estudiante = Estudiante::findOrFail($id);
        return view('Estudiantes.show', compact('estudiante'));
    }

    /**
     * Show the form for editing the specified resource.
     *  
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $carreras = Carrera::all();
        $estudiante = Estudiante::findOrFail($id);
        return view('Estudiantes.edit', compact('estudiante', 'carreras'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Estudiante  $estudiante
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $estudiante = Estudiante::findOrFail($id);
        $estudiante->update($request->all());

        Alert::success('Registro Editado Exitoso');

        return redirect()->route('estudiantes');
    }

}
