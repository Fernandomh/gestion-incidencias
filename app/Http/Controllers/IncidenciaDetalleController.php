<?php

namespace App\Http\Controllers;

use App\IncidenciaDetalle;
use App\Incidencias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class IncidenciaDetalleController extends Controller
{
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge(['user_id' => Auth::user()->id]);
        $request->merge(['estado' => $request->estado_id]);

        $detalle = IncidenciaDetalle::create($request->all());
        
        $incidencia = Incidencias::findOrFail($request->incidencia_id);
        $incidencia->estado_id = $request->estado_id;
        $incidencia->save();

        Alert::success('Incidencia Atentida');

        return redirect()->route('indexIncidenciasPendientes');
    }

}
