<?php

namespace App\Http\Controllers;

use App\Docente;
use App\EstadoIncidencia;
use App\Estudiante;
use App\IncidenciaDetalle;
use App\Incidencias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class IncidenciasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->role_id == 3) {           

            $noAsignadas = Incidencias::where('user_id', $user->id)->where('user_asignado_id', null)->get();
            $noResueltas = Incidencias::where('user_id', $user->id)->where('estado_id', 1)->get();
            $resueltas = Incidencias::where('user_id', $user->id)->where('estado_id', 2)->get();
            $pausadas = Incidencias::where('user_id', $user->id)->where('estado_id', 3)->get();  

        } else if ($user->role_id == 2) {

            $docente = Docente::where('user_id', $user->id)->first();

            $noAsignadas = Incidencias::where('carrera_id', $docente->carrera_id)->where('user_asignado_id', null)->get();
            $noResueltas = Incidencias::where('carrera_id', $docente->carrera_id)->where('estado_id', 1)->where('user_asignado_id', "!=", null)->get();
            $resueltas = Incidencias::where('carrera_id', $docente->carrera_id)->where('estado_id', 2)->get();
            $pausadas = Incidencias::where('carrera_id', $docente->carrera_id)->where('estado_id', 3)->get();  

        } else {
            
            $noAsignadas = Incidencias::where('user_asignado_id', null)->get();
            $noResueltas = Incidencias::where('estado_id', 1)->where('user_asignado_id', "!=", null)->get();
            $resueltas = Incidencias::where('estado_id', 2)->get();
            $pausadas = Incidencias::where('estado_id', 3)->get();  
        }

        return view('Incidencias.index', compact('noAsignadas', 'noResueltas', 'resueltas', 'pausadas'));
    }

    public function asignacionesPendientes()
    {
        $user = Auth::user();

        $incidencias = Incidencias::where('user_asignado_id', $user->id)->where('estado_id', "!=", 2)->get();
        return view('Incidencias.indexIncidenciasPendientes', compact('incidencias'));
    }

    public function asignacionesResueltas()
    {
        $user = Auth::user();

        $incidencias = Incidencias::where('user_asignado_id', $user->id)->where('estado_id', "=", 2)->get();
        return view('Incidencias.indexIncidenciasResueltas', compact('incidencias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $iduser = Auth::user()->id;
        $estudiante = Estudiante::where('user_id', $iduser)->first();
        return view('Incidencias.create', compact('estudiante'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user()->id;
        $estudiante = Estudiante::where('user_id', $user)->first();

        $request->merge(['user_id' => $user]);
        $request->merge(['carrera_id' => $estudiante->carrera_id]);
        $request->merge(['estado_id' => 1]);

        $incidencia = Incidencias::create($request->all());

        Alert::success('Registro Exitoso');

        return redirect()->route('incidencias');
    }

    public function asignarIncidencia(Request $request)
    {
        $incidencia = Incidencias::findOrFail($request->incidencia_id);
        $incidencia->user_asignado_id = Auth::user()->id;
        $incidencia->save();

        Alert::success('Asignación realizada con éxito');

        return redirect()->route('incidencias');
    }

    public function show($id)
    {
        $incidencia = Incidencias::findOrFail($id);
        $detalles = IncidenciaDetalle::where('incidencia_id', $id)->get();
        return view('Incidencias.show', compact('incidencia', 'detalles'));
    }

    public function estados()
    {
        return EstadoIncidencia::all();
    }
}
