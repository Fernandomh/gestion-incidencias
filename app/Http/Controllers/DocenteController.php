<?php

namespace App\Http\Controllers;

use App\Carrera;
use App\Docente;
use App\Profesion;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use PhpParser\Comment\Doc;
use RealRashid\SweetAlert\Facades\Alert;

class DocenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $docentes = Docente::all();
        return view('Docentes.index', compact('docentes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profesiones = Profesion::all();
        $carreras = Carrera::all();
        return view('Docentes.create', compact('profesiones', 'carreras'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $user = User::create([
            'role_id' => 2,
            'name' => $request->nombre,
            'email' => $request->correo,
            'password' => Hash::make($request->carnet),
        ]);
       
        $request->merge(['user_id' => $user->id]);       
        $docente = Docente::create($request->all());
        
        Alert::success('Registro Exitoso');
        
        return redirect()->route('docentes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Docente  $docente
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $docente = Docente::findOrFail($id);
        return view('Docentes.show', compact('docente'));
    }

    /**
     * Show the form for editing the specified resource.
     *  
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $profesiones = Profesion::all();
        $carreras = Carrera::all();
        $docente = Docente::findOrFail($id);
        return view('Docentes.edit', compact('profesiones', 'docente', 'carreras'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Docente  $docente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $docente = Docente::findOrFail($id);
        $docente->update($request->all());

        Alert::success('Registro editado con éxito');

        return redirect()->route('docentes');
    }
}
