<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrera extends Model
{
    protected $fillable = [
        'nombre', 'descripcion'
    ];

    protected $table = "carreras";

    protected $primarykey = "id";

    public function estudiante()
    {
        return $this->hasMany(Estudiante::class);
    }

    public function docente()
    {
        return $this->hasMany(Docente::class);
    }
}
